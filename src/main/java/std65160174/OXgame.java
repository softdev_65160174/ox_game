/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package std65160174;

/**
 *
 * @author Nobpharat
 */
import java.util.Scanner;
import java.util.Random;

public class OXgame {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rd = new Random();
        char position = ' ';
        char com, turn;
        com = ' ';
        char answer = ' ';
        String status = "Som wins";
        System.out.println("Welcome To OX GAME!!!!");

        while (true) {
            int count = 1;
            char[][] board = new char[3][3];

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    board[i][j] = Character.forDigit(count, 10);
                    count++;
                }
            }
            outBoard(board);
            System.out.print("Choose X or O:");
            turn = seLectox(sc);
            while (true) {

                System.out.print(turn + "  Turn Enter Number[1-9] : ");
                position = inPut(sc);
                changFinput(turn, position, board, sc);
                outBoard(board);
                status = checkStatus(board);
                if (!status.equals("continue")) {
                    break;
                }
                turn = changeTurn(turn);
                com = rdCom(board, position, turn, rd, com);
                System.out.println(turn + " Turn : " + com);
                outBoard(board);
                status = checkStatus(board);
                if (!status.equals("continue")) {
                    break;
                }
                turn = changeTurn(turn);
            }

            if (status.equals("Draw!")) {
                System.out.print("Game Over . Draw!");
            } else {
                System.out.println("Game Over. " + status + " Wins");
            }

            while (true) {
                System.out.print("Want to play again?  (Y or N) : ");
                String answeString = sc.next();
                if (answeString.equalsIgnoreCase("Y") || answeString.equalsIgnoreCase("N")) {
                    answer = answeString.toUpperCase().charAt(0);
                    break;
                }
                System.out.println("to start the game. Please choose Y or N.");
            }
            if (answer == 'N') {
                System.out.println("Thank you. Goodbye!");
                break;
            } else {
                System.out.println("Welcome To OX GAME Again!!!!!!");
            }
        }

    }

    private static void outBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            if (i < 2) {
                System.out.println("-----------");
            }
        }
    }

    private static char seLectox(Scanner sc) {
        while (true) {
            String input = sc.next();
            if (input.length() == 1) {
                char turn = input.charAt(0);
                if (turn == 'O' || turn == 'X') {
                    return turn;
                }
            }
            System.out.print("Invalid input. Please choose X or O. ");
        }
    }

    private static char inPut(Scanner sc) {
        while (true) {
            String input = sc.next();
            if (input.length() == 1) {
                char position = input.charAt(0);
                if (position >= '1' && position <= '9') {

                    return position;
                }
            }
            System.out.print("Invalid input. Please enter 1-9 : ");
        }
    }

    private static char changeTurn(char turn) {
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
        return turn;

    }

    private static char rdCom(char[][] board, char position, char turn, Random rd, char com) {
        while (true) {
            int i = rd.nextInt(3);
            int j = rd.nextInt(3);
            if (board[i][j] != 'X' && board[i][j] != 'O') {
                com = board[i][j];
                board[i][j] = turn;
                return com;

            }
        }
    }

    private static void changFinput(char turn, char position, char[][] board, Scanner sc) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == position) {
                    board[i][j] = turn;
                    return;
                }
            }
        }
        System.out.println("Invalid position. Please try again.");
        char newPosition = inPut(sc);
        changFinput(turn, newPosition, board, sc);
    }

    public static String checkStatus(char[][] board) {

        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                return String.valueOf(board[i][0]);
            }
        }

        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j]) {
                return String.valueOf(board[0][j]);
            }
        }

        if ((board[0][0] == board[1][1] && board[1][1] == board[2][2])
                || (board[0][2] == board[1][1] && board[1][1] == board[2][0])) {
            return String.valueOf(board[1][1]);
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] != 'X' && board[i][j] != 'O') {
                    return "continue";
                }
            }
        }

        return "Draw!";
    }

}
